<?php

namespace App\Importer;

/**
 * @author Nathan De Pachtere
 */
class ImporterContext
{
    public $type;

    public function __construct($type)
    {
        $this->type = $type;
    }
}