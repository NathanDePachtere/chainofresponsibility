<?php

namespace App\Importer;

/**
 * @author Nathan De Pachtere
 */
class CImporter implements ImporterInterface
{

    public function support(ImporterContext $importerContext)
    {
        return $importerContext->type == "C";
    }

    public function import(ImporterContext $importerContext)
    {
        echo "C Importer\n";
    }
}