<?php

namespace App\Importer;

/**
 * @author Nathan De Pachtere
 */
class ImporterChain implements ImporterInterface
{

    /**
     * @var ImporterInterface[]
     */
    private $importers;

    public function __construct(array $objects)
    {
        $this->importers = $objects;
    }

    public function support(ImporterContext $importerContext)
    {
        return true;
    }

    public function import(ImporterContext $importerContext)
    {
        foreach ($this->importers as $collector) {
            if ($collector->support($importerContext)) {
                return $collector->import($importerContext);
            }
        }

        throw new \Exception('No importers found for this context: ' . $importerContext->type);
    }
}