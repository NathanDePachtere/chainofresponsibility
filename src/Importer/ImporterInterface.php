<?php

namespace App\Importer;

/**
 * @author Nathan De Pachtere
 */

interface ImporterInterface
{

    const TAG = "app.importer";

    public function support(ImporterContext $importerContext);

    public function import(ImporterContext $importerContext);
}