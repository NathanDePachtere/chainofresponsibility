<?php

namespace App\Importer;

/**
 * @author Nathan De Pachtere
 */
class AImporter implements ImporterInterface
{

    public function support(ImporterContext $importerContext)
    {
        return $importerContext->type == "A";
    }

    public function import(ImporterContext $importerContext)
    {
        echo "A Importer\n";
    }
}