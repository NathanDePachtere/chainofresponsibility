<?php

namespace App\Importer;

/**
 * @author Nathan De Pachtere
 */
class BImporter implements ImporterInterface
{

    public function support(ImporterContext $importerContext)
    {
        return $importerContext->type == "B";
    }

    public function import(ImporterContext $importerContext)
    {
        echo "B Importer\n";
    }
}