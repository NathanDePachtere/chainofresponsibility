<?php

namespace App\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @author Nathan De Pachtere
 */
class AppCompilerPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    private $tag;
    private $class;

    public function __construct($tag, $class)
    {
        $this->tag = $tag;
        $this->class = $class;
    }

    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {
        $container
            ->findDefinition($this->class)
            ->replaceArgument('$objects', $this->findAndSortTaggedServices($this->tag, $container));
    }
}