<?php

namespace App\Command;

use App\Importer\ImporterContext;
use App\Importer\ImporterInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CorTestCommand extends Command
{
    protected static $defaultName = 'cor:test';

    /**
     * @var ImporterInterface
     */
    private $importer;

    public function __construct(string $name = null, ImporterInterface $importer)
    {
        parent::__construct($name);
        $this->importer = $importer;
    }

    protected function configure()
    {
        $this
            ->setDescription('Only for testing Chain Of Responsibility')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $importerContext = new ImporterContext("A");
        $this->importer->import($importerContext);

        $importerContext->type = "C";
        $this->importer->import($importerContext);

        $importerContext->type = "B";
        $this->importer->import($importerContext);

        try{
            $importerContext->type = "D";
            $this->importer->import($importerContext);
        } catch (\Exception $exception){
            dump($exception->getMessage());
        }


        return 0;
    }
}
